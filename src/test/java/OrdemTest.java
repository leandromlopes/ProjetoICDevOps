import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import io.micrometer.prometheus.PrometheusConfig;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import io.prometheus.client.exporter.*;
import com.sun.net.httpserver.HttpServer;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OrdemTest<HttpServlet> {
	
	public static int contador = 0;

	@Test
	public void inicia(){
		contador = 1;

		PrometheusMeterRegistry prometheusRegistry = new PrometheusMeterRegistry(PrometheusConfig.DEFAULT);
		//HttpServlet metricsServlet = new MetricsServlet(prometheusRegistry.getPrometheusRegistry());
	}
	
	@Test
	public void verifica(){
		Assert.assertEquals(1, contador);
	}
}
