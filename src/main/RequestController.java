import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.endpoint.web.annotation.RestControllerEndpoint;

@RestController
@RequestMapping("/request")
public class RequestController{

    @Value("${mensagem.requisicao}")
    private String mensagem;

    private static final Logger LOG = LoggerFactory.getLogger(RequestController.class);

    @GetMapping
    public ResponseEntity<Retorno> request()
    {
        LOG.info("teste log");
        return ResponseEntity.ok(new Retorno(mensagem, 1L));
    }
}